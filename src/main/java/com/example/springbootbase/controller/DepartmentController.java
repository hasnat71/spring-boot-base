package com.example.springbootbase.controller;

import com.example.springbootbase.dto.DepartmentDto;
import com.example.springbootbase.dto.StudentDto;
import com.example.springbootbase.entity.Department;
import com.example.springbootbase.entity.Student;
import com.example.springbootbase.service.DepartmentService;
import com.example.springbootbase.service.StudentService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartmentController {
    private final DepartmentService departmentService;
    private final StudentService studentService;

    public DepartmentController(DepartmentService departmentService, StudentService studentService) {
        this.departmentService = departmentService;
        this.studentService = studentService;
    }

    @GetMapping("/add")
    public String getDepartmentAddPage(Model model) {
        model.addAttribute("title", "Add New Department & Show All");
        model.addAttribute("departmentDto", new DepartmentDto());
        List<Department> departmentList = departmentService.getDepartmentList();
        model.addAttribute("departmentDtoList", generateDepartmentDtoList(departmentList));
        return "department/add";

    }

    @PostMapping("/save")
    public String saveDepartment(@ModelAttribute DepartmentDto departmentDto) {
        Department department = new Department();
        BeanUtils.copyProperties(departmentDto, department);
        departmentService.save(department);
        return "redirect:/department/add";
    }

    @GetMapping("/assign-student")
    public String getAssignStudentPage(Model model) {
        model.addAttribute("title", "Add New Department & Show All");

        model.addAttribute("departmentDto", new DepartmentDto());
        List<Department> departmentList = departmentService.getDepartmentList();
        model.addAttribute("departmentDtoList", generateDepartmentDtoList(departmentList));

        model.addAttribute("studentDto", new StudentDto());
        List<Student> studentList = studentService.getAllEnabled();
        model.addAttribute("studentDtoList", generateStudentDtoList(studentList));

        return "department/assign-student";
    }

    @PostMapping("/assign")
    public String assignStudent(@ModelAttribute DepartmentDto departmentDto) {
        Department department = departmentService.getDepartments(departmentDto.getDepartmentId());
        departmentDto.setDepartmentName(department.getDepartmentName());
        departmentDto.setDepartmentCode(department.getDepartmentCode());
        BeanUtils.copyProperties(departmentDto, department);
        department.setStudentList(studentListForm(departmentDto.getStudentIdList()));
        departmentService.save(department);
        return "redirect:/department/assign-student";
    }





    /*---------- helper Method-----------------*/

    private List<DepartmentDto> generateDepartmentDtoList(List<Department> DepartmentList) {
        List<DepartmentDto> DepartmentDtoList = new ArrayList<>();
        for (Department Department : DepartmentList) {
            DepartmentDto DepartmentDto = new DepartmentDto();
            BeanUtils.copyProperties(Department, DepartmentDto);
            DepartmentDtoList.add(DepartmentDto);
        }
        return DepartmentDtoList;
    }

    private List<StudentDto> generateStudentDtoList(List<Student> StudentList) {
        List<StudentDto> StudentDtoList = new ArrayList<>();
        for (Student Student : StudentList) {

            StudentDto StudentDto = new StudentDto();
            BeanUtils.copyProperties(Student, StudentDto);
            StudentDtoList.add(StudentDto);
        }
        return StudentDtoList;
    }

//    ------------- for assign student in Department --------------------

    private List<Student> studentListForm(List<Long> studentIdList) {
        List<Student> studentList = new ArrayList<>();
        for (long studentId : studentIdList) {
            Student student = studentService.getStudentById(studentId);
            studentList.add(student);

        }
        return studentList;
    }

}
