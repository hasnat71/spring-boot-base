package com.example.springbootbase.controller;

import com.example.springbootbase.dto.DepartmentDto;
import com.example.springbootbase.dto.StudentDto;
import com.example.springbootbase.entity.Department;
import com.example.springbootbase.entity.Student;
import com.example.springbootbase.service.StudentService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/add")
    public String getStudentAddPage(Model model) {
        model.addAttribute("title", "Add New Student");
        model.addAttribute("studentDto", new StudentDto());
        model.addAttribute("genderList", genderList());
        return "student/add";
    }

    @PostMapping("/save")
    public String saveStudent(@ModelAttribute StudentDto studentDto) {
        Student student = new Student();
        BeanUtils.copyProperties(studentDto, student);
        studentService.saveStudent(student);
        return "redirect:/student/getAll/";
    }

    @GetMapping("/delete/{studentId}")
    public String deleteStudent(@PathVariable(name = "studentId") long studentId) {
        Student student = studentService.getStudentById(studentId);
        student.setEnable(false);
        studentService.saveStudent(student);
        return "redirect:/student/getAll";
    }


    @GetMapping("/getAll")
    public String getAllStudents(Model model) {
        List<Student> studentList = studentService.getAllStudent();
        List<StudentDto> studentDtoList = generateStudentDtoList(studentList);

        model.addAttribute("studentDtoList", studentDtoList);

        return "student/get-all";
    }

    @GetMapping("/update/{studentId}")
    public String updateStudent(@PathVariable(name = "studentId") long studentId, Model model) {
        StudentDto studentDto = new StudentDto();
        Student student = studentService.getStudentById(studentId);
        BeanUtils.copyProperties(student, studentDto);
        model.addAttribute("studentDto", studentDto);
        model.addAttribute("genderList", this.genderList());
        return "student/add";
    }


    /*---------------------------- Helper Method---------------------------------*/
    private List<String> genderList() {
        List<String> genderList = new ArrayList<>();
        genderList.add("Male");
        genderList.add("Female");
        return genderList;
    }

//    ---------- Generating StudentDot to show on get page --------------

    private List<StudentDto> generateStudentDtoList(List<Student> studentList) {
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student : studentList) {
            StudentDto studentDto = new StudentDto();
            BeanUtils.copyProperties(student, studentDto);
            if (student.getDepartment() == null) {
                DepartmentDto departmentDto = new DepartmentDto();
                departmentDto.setDepartmentCode("not Assign Yet");
                studentDto.setDepartmentDto(departmentDto);
            } else {
                studentDto.setDepartmentDto(getDepartmentDto(student));
            }
            studentDtoList.add(studentDto);
        }
        return studentDtoList;
    }


    private DepartmentDto getDepartmentDto(Student student) {
        DepartmentDto departmentDto = new DepartmentDto();
        BeanUtils.copyProperties(student.getDepartment(), departmentDto);
        return departmentDto;
    }


}

