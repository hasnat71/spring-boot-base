package com.example.springbootbase.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class StudentDto {
    private long studentId;
    private String studentName;
    private int age;
    private String gender;

    private  DepartmentDto departmentDto;

    private boolean enable=true;
}

