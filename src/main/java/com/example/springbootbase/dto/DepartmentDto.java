package com.example.springbootbase.dto;

import com.example.springbootbase.entity.Student;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DepartmentDto {
    private long departmentId;
    private String departmentName;
    private String departmentCode;
    private List<Student> studentList;

    private List<Long> studentIdList;
}
