package com.example.springbootbase.service;

import com.example.springbootbase.entity.Subject;
import com.example.springbootbase.repository.SubjectRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    private final SubjectRepo subjectRepo;

    public SubjectService(SubjectRepo subjectRepo) {
        this.subjectRepo = subjectRepo;
    }

    public void saveSubject(Subject subject) {
        subjectRepo.save(subject);
    }
    public List<Subject> getAllSubject() {
        return subjectRepo.findAll();
    }

}
