package com.example.springbootbase.service;

import com.example.springbootbase.entity.Student;
import com.example.springbootbase.repository.StudentRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepo studentRepo;

    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    public void saveStudent(Student student) {
        studentRepo.save(student);
    }

    public List<Student> getAllEnabled() {
        List<Student> studentList = studentRepo.findAllByEnableTrue();
        return studentList;
    }

    public Student getStudentById(long studentId) {
        return studentRepo.getOne(studentId);
    }

    public List<Student> getAllStudent() {
        return studentRepo.findAllByEnableTrue();
    }

}
