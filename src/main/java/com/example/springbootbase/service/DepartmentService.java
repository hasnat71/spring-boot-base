package com.example.springbootbase.service;

import com.example.springbootbase.dto.DepartmentDto;
import com.example.springbootbase.entity.Department;
import com.example.springbootbase.repository.DepartmentRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {
    private final DepartmentRepo departmentRepo;

    public DepartmentService(DepartmentRepo departmentRepo) {
        this.departmentRepo = departmentRepo;
    }

    public void save(Department department) {
        departmentRepo.save(department);
    }

    public List<Department> getDepartmentList() {
        return departmentRepo.findAll();
    }

    public Department getDepartments(long departmentId) {
        return departmentRepo.getOne(departmentId);
    }



    //depart return korabo
    //id diye department tulbe, return department dto


}
